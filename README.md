# Run docker compose

~~~
cd ./core
docker compose -f docker-compose_develop.yml up -d --build --force-recreate --remove-orphans --renew-anon-volumes
~~~

# Stop docker compose

~~~
docker compose -f docker-compose_develop.yml down
~~~